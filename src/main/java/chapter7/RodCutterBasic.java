package chapter7;

import static chapter7.Memoizer.callMemoized;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class RodCutterBasic {
    final List<Integer> prices;

    static final List<Integer> priceValues = Arrays.asList(2, 1, 1, 2, 2, 2, 1, 8, 9, 15);

    public RodCutterBasic(final List<Integer> pricesForLength) {
        prices = pricesForLength;
    }

    // O(2^n) style
//    public int maxProfit(final int length) {
//        int profit = (length <= prices.size() ? prices.get(length - 1) : 0);
//        for (int i = 1; i < length; i++) {
//            int priceWhenCut = maxProfit(i) + maxProfit(length - i);
//            if (profit < priceWhenCut) {
//                profit = priceWhenCut;
//            }
//        }
//        return profit;
//    }

    // speeding up with memoization
    public int maxProfit(final int rodLength) {
        BiFunction<Function<Integer, Integer>, Integer, Integer> compute =
                (func, length) -> {
                    int profit = (length <= prices.size()) ? prices.get(length - 1) : 0;
                    for (int i = 1; i < length; i++) {
                        int priceWhenCut = func.apply(i) + func.apply(length - i);
                        if (profit < priceWhenCut) {
                            profit = priceWhenCut;
                        }
                    }
                    return profit;
                };
        return callMemoized(compute, rodLength);
    }

    public static void run(final RodCutterBasic rodCutter) {
        System.out.println(rodCutter.maxProfit(5));
        System.out.println(rodCutter.maxProfit(22));
    }

    public static void main(String[] args) {
        final RodCutterBasic rodCutter = new RodCutterBasic(priceValues);
        run(rodCutter);
    }

}
