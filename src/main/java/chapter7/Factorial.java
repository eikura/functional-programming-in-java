package chapter7;

import static chapter7.TailCalls.call;
import static chapter7.TailCalls.done;

public class Factorial {
    public static int factorialRec(final int number) {
        if (number == 1) {
            return number;
        } else {
            return number * factorialRec(number - 1);
        }
    }

    public static TailCall<Integer> factorialTailRec(final int factorial, final int number) {
        if (number == 1) {
            return done(factorial);
        } else {
            return call(() -> factorialTailRec(factorial * number, number - 1));
        }
    }

    public static void main(String[] args) {
        System.out.println(factorialTailRec(1, 10).invoke());
    }
}
