package chapter4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;

public class YahooFinance {
    public static BigDecimal getPrice(final String ticker) {
        try {
            final URL url = new URL(
                    "https://query1.finance.yahoo.com/v8/finance/chart/" + ticker + "?interval=1d");

            final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            final String data = reader.lines().findFirst().get();
            final String[] dataItems = data.split(",");
            return new BigDecimal(dataItems[8].split(":")[1]);

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
}
