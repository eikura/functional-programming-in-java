package chapter4;

public class MailerBuilder {
    public MailerBuilder from(final String address){
        return this;
    }
    public MailerBuilder to(final String address){
        return this;
    }
    public MailerBuilder subject(final String line){
        return this;
    }
    public MailerBuilder body(final String message){
        return this;
    }
    public void send() {
        System.out.println("sending...");
    }

    public static void main(String[] args) {
        new MailerBuilder().from("build@agiledeveloper.com")
                            .to("venkats@agiledeveloper.com")
                            .subject("build notification")
                            .body("... it sucks less...")
                            .send();
    }
}
