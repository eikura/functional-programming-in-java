package chapter3;

import chapter3.Person;
import java.util.*;
import java.util.function.Function;


import static java.util.stream.Collectors.toList;
import static java.util.Comparator.comparing;



public class Compare{

    public static void printPeople(final String message, final List<Person> people){
        System.out.println(message);
        people.forEach(System.out::println);
    }

    public static void main(String[] args) {
     
        final List<Person> people = Arrays.asList(
            new Person("John", 20),
            new Person("Sara", 21),
            new Person("Jane", 21),
            new Person("Greg", 35)
        );

        // basic comparator form
        List<Person> ascendingAge1 = people.stream()
                                        .sorted((person1, person2) -> person1.ageDifference(person2))
                                        .collect(toList());
        List<Person> descendingAge1 = people.stream()
                                        .sorted((person1, person2) -> person2.ageDifference(person1))
                                        .collect(toList());

        // short comparator form
        List<Person> ascendingAge2 = people.stream()
                                            .sorted(Person::ageDifference)
                                            .collect(toList());

        Comparator<Person> compareAscending = (person1, person2) -> person1.ageDifference(person2);
        Comparator<Person> compareDescending = compareAscending.reversed();

        List<Person> descendingAge2 = people.stream()
                                            .sorted(compareDescending)
                                            .collect(toList());
        
        // min max example
        people.stream()
                .min(Person::ageDifference)  // It returns Optional
                .ifPresent(System.out::println);
                                    
        printPeople("Sorted in ascending order by age: ", descendingAge2);

        // sort by two properties
        final Function<Person, Integer> byAge = person -> person.getAge();
        final Function<Person, String> byName = person -> person.getName();

        printPeople("Sorted in ascending order by age and name ", 
        people.stream()
              .sorted(comparing(byAge).thenComparing(byName))
              .collect(toList()));
    }
}