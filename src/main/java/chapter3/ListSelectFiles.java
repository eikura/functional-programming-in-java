package chapter3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ListSelectFiles{
    public static void main(String[] args) {

        // old form
        final String[] files =
            new File("chapter3").list(new java.io.FilenameFilter(){
                public boolean accept(final File dir, final String name){
                    return name.endsWith(".java");
                }
            });
        System.out.println(files);

        // functional form
        try{
            Files.newDirectoryStream(Paths.get("chapter3"), path -> path.toString().endsWith(".java"))
            .forEach(System.out::println);
        } catch(IOException ex){
            ex.printStackTrace();
        }
    }
}