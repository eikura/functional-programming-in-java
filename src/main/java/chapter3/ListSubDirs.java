package chapter3;

import java.io.File;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ListSubDirs{
    public static void main(String[] args) {
        listTheHardWay();
        betterWay();
    }


    public static void listTheHardWay(){
        List<File> files = new ArrayList<>();

        File[] filesInCurrentDir = new File(".").listFiles();
        for(File file: filesInCurrentDir) {
            File[] filesInSubDir = file.listFiles();
            if(filesInSubDir != null){
                files.addAll(Arrays.asList(filesInSubDir));
            }else{
                files.add(file);
            }
        }
        System.out.println("Count: " + files.size());
    }

    public static void betterWay(){
        
        List<File> files = Stream.of(new File(".").listFiles())
                                .flatMap(file -> file.listFiles() == null ? // 계층구조로 생기는 stream들을 flatMap으로 flatten한 후 collect
                                            Stream.of(file) : Stream.of(file.listFiles()))
                                .collect(toList());
        System.out.println("Count: " + files.size());   
    }
}