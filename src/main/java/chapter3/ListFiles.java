package chapter3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ListFiles{
    public static void main(String[] args) {
        try{
            Files.list(Paths.get(".")).
            filter(Files::isDirectory).
            forEach(System.out::println);
        }catch(IOException ex){
            ex.printStackTrace();
        }
        
    }
}