package chapter3;

public class Person{
    private final String name;
    private final int age;

    public Person(final String theName, final int theAge){
        this.name = theName;
        this.age = theAge;
    }

    public String getName(){return name;}
    public int getAge(){return age;}

    public int ageDifference(final Person other){
        return this.age - other.getAge();
    }

    public String toString(){
        return String.format("%s - %d", name, age);
    }
}