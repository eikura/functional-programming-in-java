package chapter3;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import chapter3.Person;
import static java.util.stream.Collectors.*;

public class OlderThan20{
    public static void main(String[] args) {
        final List<Person> people = Arrays.asList(
            new Person("John", 20),
            new Person("Sara", 21),
            new Person("Jane", 21),
            new Person("Greg", 35)
        );
        
        // ArrayList form
        // List<Person> olderThan20 = 
        // people.stream()
        //       .filter(person -> person.getAge() > 20)
        //       .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        // System.out.println("People older than 20: " + olderThan20);

        // Collector form
        // List<Person> olderThan20 = 
        // people.stream()
        //       .filter(person -> person.getAge() > 20)
        //       .collect(toList());
        // System.out.println("People older than 20: " + olderThan20);

        // Another operation
        // Map<Integer, List<Person>> olderThan20 = 
        // people.stream()
        //       .filter(person -> person.getAge() > 20)
        //       .collect(groupingBy(Person::getAge));
        // System.out.println("People older than 20: " + olderThan20);


        // group and show specific property (name in this case) only.
        Comparator<Person> byAge = Comparator.comparing(Person::getAge);
        Map<Character, Optional<Person>> oldestPersonOfEachLetter =
        people.stream()
              .collect(groupingBy(person->person.getName().charAt(0), reducing(BinaryOperator.maxBy(byAge))));
        System.out.println("Oldest person of each letter: ");
        System.out.println(oldestPersonOfEachLetter);
    }
}