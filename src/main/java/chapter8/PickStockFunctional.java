package chapter8;

import java.sql.Time;
import java.util.Timer;
import java.util.stream.Stream;

public class PickStockFunctional {

    public static void findHighPriced(final Stream<String> symbols){
        final StockInfo highPriced =
                symbols.map(StockUtil::getPrice)
                        .filter(StockUtil.isPriceLessThan(500))
                        .reduce(StockUtil::pickHigh)
                        .get();

        System.out.println("High priced under $500 is " + highPriced);
    }

    public static void main(String[] args) {
        float startTime = System.nanoTime();
//        findHighPriced(Tickers.symbols.stream());
        findHighPriced(Tickers.symbols.parallelStream());
        float duration = (System.nanoTime() - startTime) / 1000000000f;
        System.out.println("The method took " + duration + " seconds");
    }
}
