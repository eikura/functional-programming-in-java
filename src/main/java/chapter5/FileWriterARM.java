package chapter5;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterARM implements AutoCloseable {
    private final FileWriter writer;

    public FileWriterARM(final String fileName) throws IOException {
        writer = new FileWriter(fileName);
    }
    public void writeStuff(final String message) throws IOException{
        writer.write(message);
    }
    public void close() throws IOException{
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        try(final FileWriterARM fileWriterARM = new FileWriterARM("peekaboo.txt")){
            fileWriterARM.writeStuff("peek-a-boooo");
            System.out.println("done with the resource...");
        }
    }
}
