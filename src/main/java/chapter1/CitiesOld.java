package chapter1;

import java.util.*;

public class CitiesOld{
    
    List<String> cities = Arrays.asList("Albany", "Boulder", "Chicago", "Denver", "Eugene");

    private void findCity(String name){
        boolean found = false;
        for(String city : cities){
            if(city.equals(name)){
                found = true;
                break;
            }
        }
        System.out.println(Boolean.valueOf(found).toString());
    }

    public static void main(String[] args) {
        CitiesOld citiesOld = new CitiesOld();

        citiesOld.findCity("Chicago");        
    }

}