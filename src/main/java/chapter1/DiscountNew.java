package chapter1;

import java.math.BigDecimal;
import java.util.*;

public class DiscountNew{
    public static final List<BigDecimal> prices = Arrays.asList(
      new BigDecimal("10"), new BigDecimal("30"), new BigDecimal("17"),
      new BigDecimal("20"), new BigDecimal("15"), new BigDecimal("18"),
      new BigDecimal("45"), new BigDecimal("12"));


    public void totalPrice(){
        final BigDecimal totalOfDiscountedPrices =
        prices.stream()
                .filter(price -> price.compareTo(BigDecimal.valueOf(20)) > 0)
                .map(price -> price.multiply(BigDecimal.valueOf(0.9)))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Total of discounted prices: " + totalOfDiscountedPrices);
    }

    public static void main(String[] args) {
        DiscountNew discountNew = new DiscountNew();

        discountNew.totalPrice();
    }
}