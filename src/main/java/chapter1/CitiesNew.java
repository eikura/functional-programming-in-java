package chapter1;

import java.util.*;

public class CitiesNew{
    
    List<String> cities = Arrays.asList("Albany", "Boulder", "Chicago", "Denver", "Eugene");

    private void findCity(String name){
        System.out.println(cities.contains(name));
    }

    public static void main(String[] args) {
        CitiesNew citiesNew = new CitiesNew();

        citiesNew.findCity("Chicago");        
    }

}