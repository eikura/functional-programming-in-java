package chapter2;

import java.util.*;
import static java.util.stream.Collectors.joining;


public class PrintList{
    final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {
        PrintList printList = new PrintList();

        System.out.println(printList.friends.stream()
                                            .map(String::toUpperCase)
                                            .collect(joining(", ")));
    }
}