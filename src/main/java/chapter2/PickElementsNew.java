package chapter2;

import java.util.*;
import java.util.stream.Collectors;

public class PickElementsNew{

    final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {
        PickElementsNew pickElementsNew = new PickElementsNew();

        final List<String> startsWithN = pickElementsNew.friends.stream()
                                                                .filter(name -> name.startsWith("N"))
                                                                .collect(Collectors.toList());

        System.out.println(startsWithN);

    }

}