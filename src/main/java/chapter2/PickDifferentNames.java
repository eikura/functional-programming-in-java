package chapter2;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class PickDifferentNames{
    final List<String> friends = Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
    final List<String> editors = Arrays.asList("Brian", "Jackie", "John", "Mike");
    final List<String> comrades = Arrays.asList("Kate", "Ken", "Nick", "Paula", "Zach");
    public static void main(String[] args) {
        PickDifferentNames pickDifferentNames = new PickDifferentNames();

        // the way to pass the Predicate for the filter, way 1
        // final Predicate<String> startsWithN = name -> name.startsWith("N");
        // final Predicate<String> startsWithB = name -> name.startsWith("B");

        // final long countFriendsStartN = pickDifferentNames.friends.stream()
        //                                                         .filter(startsWithN)
        //                                                         .count();

        // final long countFriendsStartB = pickDifferentNames.friends.stream()
        //                                                         .filter(startsWithB)
        //                                                         .count();

        // the way to pass the Predicate for the filter, way 2
        // final long countFriendsStartN = pickDifferentNames.friends.stream()
        //                                                         .filter(checkIfStartsWith("N"))
        //                                                         .count();
        // final long countFriendsStartB = pickDifferentNames.friends.stream()
        //                                                         .filter(checkIfStartsWith("B"))
        //                                                         .count();
        

        // It replace the 'checkIfStartsWith' static method to the lambda expression
        final Function<String, Predicate<String>> startsWithLetter = (String letter) -> {
            Predicate<String> checkStarts = (String name) -> name.startsWith(letter);
            return checkStarts;
        };
        // Or you can use more concise syntax like:
        final Function<String, Predicate<String>> startsWFunction = letter -> name -> name.startsWith(letter);

        final long countFriendsStartN = pickDifferentNames.friends.stream()
                                                                .filter(startsWithLetter.apply("N"))
                                                                .count();

        final long countFriendsStartB = pickDifferentNames.friends.stream()
                                                                .filter(startsWFunction.apply("B"))
                                                                .count();


        System.out.println(String.format("%d, %d", countFriendsStartN, countFriendsStartB));

    }

    public static Predicate<String> checkIfStartsWith(final String letter){
        return name -> name.startsWith(letter);
    }

    
}