package chapter2;

import java.util.*;
import java.util.function.Consumer;

public class IterationNew{
    private final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {

        IterationNew iterationNew = new IterationNew();

        // Original Form
        iterationNew.friends.forEach(new Consumer<String>(){
            public void accept(final String name){
                System.out.println(name);
            }
        });

        // Short Form 1
        iterationNew.friends.forEach((final String name) -> System.out.println(name));

        // Short Form 2
        iterationNew.friends.forEach((name) -> System.out.println(name));

        // Short Form 3
        iterationNew.friends.forEach(name -> System.out.println(name));

        // Short Form 4
        iterationNew.friends.forEach(System.out::println);
    }

}