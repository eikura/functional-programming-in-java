package chapter2;

import java.util.*;

public class IterationOld{
    private final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {

        IterationOld iterationOld = new IterationOld();

        for(String name: iterationOld.friends){
            System.out.println(name);
        }
    }

}