package chapter2;

import java.util.*;

public class PickAnElementNew{
    final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {
        PickAnElementNew pickAnElementNew = new PickAnElementNew();

        pickName(pickAnElementNew.friends, "N");
        
    }

    public static void pickName(final List<String> names, final String startingLetter){
        final Optional<String> foundName = names.stream()
                                                .filter(name -> name.startsWith(startingLetter))
                                                .findFirst();
        
        foundName.ifPresent(name -> System.out.println("Hello " + name));
    }
}