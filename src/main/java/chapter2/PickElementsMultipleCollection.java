package chapter2;

import java.util.*;
import java.util.function.Predicate;

public class PickElementsMultipleCollection{
    final List<String> friends = Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
    final List<String> editors = Arrays.asList("Brian", "Jackie", "John", "Mike");
    final List<String> comrades = Arrays.asList("Kate", "Ken", "Nick", "Paula", "Zach");
    public static void main(String[] args) {
        PickElementsMultipleCollection pickElementsMultipleCollection = new PickElementsMultipleCollection();

        // To remove the duplication of the code in .filter(~)
        final Predicate<String> startsWithN = name -> name.startsWith("N");

        final long countFriendsStartN = pickElementsMultipleCollection.friends.stream()
                                                                                .filter(startsWithN)
                                                                                .count();

        final long countEditorsStartN = pickElementsMultipleCollection.editors.stream()
                                                                                .filter(startsWithN)
                                                                                .count();

        final long countComradesStartN = pickElementsMultipleCollection.comrades.stream()
                                                                                .filter(startsWithN)
                                                                                .count();

        System.out.println(String.format("%d, %d, %d", countFriendsStartN, countEditorsStartN, countComradesStartN));


    }

}