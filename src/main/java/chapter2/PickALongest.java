package chapter2;

import java.util.*;

public class PickALongest{
    final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
    
    public static void main(String[] args) {
        PickALongest pickALongest = new PickALongest();

        System.out.println("Total number of characters in all names: "+
        pickALongest.friends.stream()
                            .mapToInt(name -> name.length())
                            .sum());

        // final Optional<String> aLongName = pickALongest.friends.stream()    
        //                                                     .reduce((name1, name2) ->
        //                                                     name1.length() >= name2.length() ? name1: name2);
        // aLongName.ifPresent(name -> System.out.println(String.format("A longest name: %s", name)));
        // "Steve" is default which makes Optional<String> to String
        final String aLongName = pickALongest.friends.stream()    
                                                            .reduce("Steve", (name1, name2) ->
                                                            name1.length() >= name2.length() ? name1: name2);
        System.out.println(String.format("A longest name: %s", aLongName));
    }
}