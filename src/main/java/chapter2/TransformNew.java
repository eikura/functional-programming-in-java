package chapter2;

import java.util.*;

public class TransformNew{
    final List<String> friends= Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
    final List<String> uppercaseNames = new ArrayList<String>();

    public static void main(String[] args) {
        TransformNew transformNew = new TransformNew();
        
        // Short form 1
        transformNew.friends.forEach(name -> transformNew.uppercaseNames.add(name.toUpperCase()));
        System.out.println(transformNew.uppercaseNames);

        // Short form 2
        transformNew.friends.stream()
                            .map(name -> name.toUpperCase())
                            .forEach(name -> System.out.print(name + " "));
        System.out.println();

        // Short form 3
        transformNew.friends.stream()
                            .map(String::toUpperCase)
                            .forEach(name -> System.out.println(name));

        // the input and the output can have different data types.
        transformNew.friends.stream()
                            .map(name -> name.length())
                            .forEach(count -> System.out.print(count + " "));
        System.out.println();
    }
}