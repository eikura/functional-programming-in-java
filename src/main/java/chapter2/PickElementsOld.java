package chapter2;

import java.util.*;

public class PickElementsOld{
    final List<String> friends = Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    public static void main(String[] args) {
        PickElementsOld pickElementsOld = new PickElementsOld();

        final List <String> startWithN = new ArrayList<String>();

        for(String name: pickElementsOld.friends){
            if(name.startsWith("N")){
                startWithN.add(name);
            }
        }

        System.out.println(startWithN);
    }
}